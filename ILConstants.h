//
//  ILConstants.h
//  InfoList
//
//  Created by Aamir Jelani on 26/02/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#ifndef InfoList_ILConstants_h
#define InfoList_ILConstants_h

#define kUpdateInfo 				@"kUpdateInfo"
#define kUpdateDetailInfo 			@"kUpdateDetailInfo"
#define kUpdateDetailInfoOnError 	@"kUpdateDetailInfoOnError"
#define kNetWorkNotFound            @"kNetWorkNotFound"

#endif
