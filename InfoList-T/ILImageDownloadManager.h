//
//  ILImageDownloadManager.h
//  InfoList
//  ILImageDownloadManager provides the functionality to retrieve images from remote servers using http connection. It is
//  derived from NSOperation and thus its object can be put in an Operation Queue for background processing.
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ILImageDownloadManager : NSOperation

//item refers to the current item being processed w.r.t. the list of items in list view
@property (nonatomic, strong) NSString *url;

//item refers to the current item being processed w.r.t. the list of items in list view
@property (nonatomic, strong) UIImage *image;

// Code that executes after successfully downloading the image
@property (nonatomic, copy) void (^completionHandler)(void);

// Code that executes if download is not successful
@property (nonatomic, copy) void (^errorHandler)(void);

@end
