//
//  ILListViewController.m
//  InfoList-T
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import "ILListViewController.h"
#import "ILListItem.h"
#import "ILServerDownloadManager.h"
#import "ILConstants.h"
#import "ILListViewCell.h"
#import "ILImageDownloadManager.h"


@interface ILListViewController () {
    BOOL isMoving;
    NSTimer *timer;
    
}

// NSOperation Queue used to register image download operations
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property(nonatomic, strong) ILListViewCell	*sizingCell;
@property (nonatomic, strong) NSMutableArray *indexPaths;

@property (nonatomic, strong) ILImageDownloadManager *operation;

// displayMessage contains message to be displayed when there is not data loaded.
@property (atomic, strong) NSString *displayMessage;

@end

@implementation ILListViewController

@synthesize displayMessage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isMoving = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Register for kUpdateInfo notification that is triggered when JSON data download is complete
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInfo) name:kUpdateInfo object:nil];
    
    // Register for kNetworkNotFound notification that is triggered when Network connection is lost
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkNotFound) name:kNetWorkNotFound object:nil];
    
    // Set the display message default value
    self.displayMessage = @"Loading Content.\n\nPlease wait";
    
    self.downloadQueue = [[[NSOperationQueue alloc] init] autorelease];
    self.downloadQueue.maxConcurrentOperationCount = MAXFLOAT;
    self.downloadQueue.name = @"Icon Download Queue";
    
    // Initialize the refresh control.
    self.refreshControl = [[[UIRefreshControl alloc] init] autorelease];
    self.refreshControl.backgroundColor = [UIColor blueColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(refreshData)
                  forControlEvents:UIControlEventValueChanged];

    /*
     * create a cell instance to use for auto layout sizing
     */
    self.sizingCell = [[ILListViewCell alloc] initWithReuseIdentifier:nil];
    self.sizingCell.autoresizingMask = UIViewAutoresizingFlexibleWidth; // this must be set for the cell heights to be calculated correctly in landscape
    self.sizingCell.hidden = YES;
    
    self.sizingCell.frame = CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 0);
    [self.tableView addSubview:self.sizingCell];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
}

- (void) orientationChanged:(NSNotification *)note
{
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) refreshData {
    if (self.refreshControl) {
        
        // Format date to display when pulling down for refresh
        NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[[NSAttributedString alloc] initWithString:title attributes:attrsDictionary] autorelease];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [ILServerDownloadManager setup];
        [ILServerDownloadManager startDownload];
    }
}

// kUpdateInfo notification handler.
- (void) updateInfo {
    
    // If this was called in response to a refresh, dismiss the refresh control
    if(self.refreshControl)
        [self.refreshControl endRefreshing];
    
    //get the title and update the table view
    self.title = [ILServerDownloadManager sharedInstance].listHeading;
    
    [self.tableView reloadData];
    
}

- (void) handleNetworkNotFound {
    self.displayMessage = @"Unable to connect to Server.\n\nPlease try again";
    
    if([self.refreshControl isRefreshing])
        [self.refreshControl endRefreshing];
    else
        [self.tableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    
    // If there is no data, just display a friendly message and return section count as 0
    // If there is data return section count as 1
    
    if ([ILServerDownloadManager sharedInstance].itemsArray  && [[ILServerDownloadManager sharedInstance].itemsArray count]) {
        
        self.tableView.backgroundView = nil;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        return 1;
        
    } else {
        
        
        // Display a message when the table is empty
        UILabel *messageLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)] autorelease];
        
        messageLabel.text = self.displayMessage;
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Helvetica-Italic" size:50];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return [[ILServerDownloadManager sharedInstance].itemsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cellID";
    
    ILListViewCell *cell = nil;
    
    NSString *cellId = nil;
    
    // choose the appropriate cell ID
    cellId = cellID;
    
    cell = [[ILListViewCell alloc] initWithReuseIdentifier:cellId];
    
    ILListItem *item = [[ILServerDownloadManager sharedInstance].itemsArray objectAtIndex:indexPath.row];
    
    [cell setData:item];
    
    [cell applyConstraints];
    
    [_indexPaths addObject:indexPath];
    
    if (!item.hasData)
    {
        [self startImageDownload:item forIndexPath:indexPath];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [((ILListViewCell*)cell) updateGradient];
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    self.downloadQueue.suspended = YES;
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate)
        self.downloadQueue.suspended = NO;
}
- (void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    self.downloadQueue.suspended = NO;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    self.downloadQueue.suspended = NO;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ILListViewCell minimumHeightShowingImage:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat calculatedHeight = 0;
    
    ILListItem *item = [[ILServerDownloadManager sharedInstance].itemsArray objectAtIndex:indexPath.row];
    
    [self.sizingCell setData:item];
    
    [self.sizingCell setNeedsLayout];
    [self.sizingCell layoutIfNeeded];
    
    calculatedHeight = [self.sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    return calculatedHeight;
    
}


// startImageDownload creates and starts execution of operation to downlod the image
- (void)startImageDownload:(ILListItem *)appRecord forIndexPath:(NSIndexPath *)indexPath
{
    if (appRecord.session) return;
    
    _operation = [[[ILImageDownloadManager alloc] init] autorelease];
    _operation.url = appRecord.imagePath;
    
    __block ILImageDownloadManager *operationWeak = _operation;
    
    // delegate to be called once operation is successfully completed
    [_operation setCompletionHandler:^{
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            // Get the image for display
            appRecord.image= operationWeak.image;
            
            // Set this so that it is not loaded again
            appRecord.hasData = YES;
            
            // Reload the corresponding cell only
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [(ILListViewCell*)[self.tableView cellForRowAtIndexPath:indexPath] setNeedsUpdateConstraints];
            [(ILListViewCell*)[self.tableView cellForRowAtIndexPath:indexPath] setNeedsLayout];
            
            
        }];
    }];
    
    // delegate to be called once operation fails to execute
    [_operation setErrorHandler:^{
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            appRecord.hasData = NO;
            appRecord.image = operationWeak.image;
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            
        }];
    }];
    
    
    appRecord.session = _operation;
    [self.downloadQueue addOperation:_operation];
}

- (void) update {
    if(timer){
        [timer invalidate];
        timer = nil;
    }
    [self.tableView reloadData];
}

// didEndDisplayingCell gets called each time a cell goes out of visible view bounds.
// Here we cancel if there is an associated image download operation in progress
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![ILServerDownloadManager sharedInstance].itemsArray) return;
    
    ILListItem *appRecord = [[ILServerDownloadManager sharedInstance].itemsArray objectAtIndex:indexPath.row];
    [_indexPaths removeObject:indexPath];
    if (appRecord.session){
        [appRecord.session cancel];
        appRecord.session = nil;
    }
}


@end
