//
//  ILJSONParser.m
//  InfoList
//
//  Created by Aamir Jelani on 22/02/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import "ILJSONParser.h"
#import "ILListItem.h"
#import "CJSONDeserializer.h"

@interface ILJSONParser ()

// This ivar will contain the json data as received from http
@property (nonatomic, strong) NSData *dataToParse;

// This ivar will contain current item under process
@property (nonatomic, strong) ILListItem *currItem;

@end
@implementation ILJSONParser

// This function is used to store data before this operation executes
- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self != nil)
    {
        self.dataToParse = data;
    }
    return self;
}

// Main function that gets called when operation executes. It performs all the parsing and stores
// all items in an array
- (void)main
{
    NSError *error;
    
    self.itemsArray = [NSMutableArray array];
    
    // Parse JSON results with TouchJSON.  It converts it into a dictionary.
    CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
    jsonDeserializer.nullObject = NULL;
    
    // Following two functions only convert the JSON data feed from ASCII to UTF8 for proper processing
    NSString *resultString = [[[NSString alloc] initWithData:self.dataToParse encoding:NSASCIIStringEncoding] autorelease];
    
    NSData *newData = [resultString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Now let's get the json data in dictionary format
    NSDictionary *outData = [jsonDeserializer deserializeAsDictionary:newData error:&error];
    
    // First let's extract the top heading
    self.listHeading = [outData objectForKey:@"title"];
    
    // Then we'll get the array of items (dictionaries) in the data
    NSArray  *array = [outData objectForKey:@"rows"];
    
    //Now for each item in array, create a custom object and store in an object array
    for (NSDictionary *item in array) {
        self.currItem = [[[ILListItem alloc] init] autorelease];
        self.currItem.title = [item objectForKey:@"title"];
        self.currItem.text = [item objectForKey:@"description"];
        self.currItem.imagePath = [item objectForKey:@"imageHref"];
        self.currItem.image = [UIImage imageNamed:@"noimage"];
        [self.itemsArray addObject:self.currItem];
        self.currItem = nil;
    }
    
    newData = nil;
    jsonDeserializer = nil;
    outData = nil;
}

- (void) dealloc {
    [super dealloc];
    self.itemsArray = nil;
    self = nil;
}


@end
