//
//  ILListItem.h
//  InfoList
// ILListItem provides the object structure to store JSON elements.
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ILImageDownloadManager.h"

@interface ILListItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) UIImage  *image;
@property (nonatomic, strong) ILImageDownloadManager *session;
@property (nonatomic) BOOL hasData;

@end
