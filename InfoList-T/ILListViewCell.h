//
//  ILListViewCell.h
//  InfoList-T
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILListItem.h"

// IListViewCell interface represents the single row in table view
@interface ILListViewCell : UITableViewCell

// Class function that returns minimum cell height
+ (CGFloat)minimumHeightShowingImage:(BOOL)showingImage;

// initialization function
- (instancetype)initWithReuseIdentifier:(NSString*)reuseIdentifier;

// setData is called by caller to define element properties
- (void)setData:(ILListItem *)item;

// applyConstraints function reinitializes the constraints
- (void) applyConstraints;

- (void) updateGradient;

@end
