//
//  ILListViewCell.m
//  InfoList-T
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILListViewCell.h"
#import "JSLabel.h"
#import "ILListItem.h"
#import "NSLayoutConstraint+ClassMethodPriority.h"

// Common constants
const CGFloat fontSize = 15.0f;
const CGFloat cellPadding = 10.0f;
const CGFloat imageWidth = 50.0f;
const CGFloat imageHeight = 40.0f;

@interface ILListViewCell()

// imageConstraints contains constraints that are applicable only when image is displayed
@property(nonatomic,strong) NSMutableArray *imageConstraints;

// container for single data record
@property(nonatomic,strong) ILListItem *item;

// Top Label
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

// Detailed description text
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;

// hidden empty label at bottom to fix description text truncation issue
@property (retain, nonatomic) IBOutlet UILabel *dummyLabel;

// image view
@property (retain, nonatomic) IBOutlet UIImageView *image;

// Wait Indicator
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@end

@implementation ILListViewCell

@synthesize titleLabel;
@synthesize descriptionLabel;
@synthesize dummyLabel;

+ (CGFloat)minimumHeightShowingImage:(BOOL)showingImage {
    if (showingImage) {
        return cellPadding +
        imageHeight +
        cellPadding +
        [@"JS" sizeWithFont:[UIFont systemFontOfSize:fontSize]].height +
        cellPadding;
    }
    
    return 88;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    self.accessoryType = UITableViewCellAccessoryNone;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Initializing title text label
    titleLabel = [[[JSLabel alloc] init] autorelease];
    titleLabel.font = [UIFont boldSystemFontOfSize:fontSize];
    titleLabel.textColor = [UIColor colorWithRed:.2f green:.4f blue:.8f alpha:1.0f];
    titleLabel.numberOfLines = 0;
    titleLabel.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:titleLabel];
    
    // Initializing description text label
    descriptionLabel = [[[JSLabel alloc] init] autorelease];
    descriptionLabel.font = [UIFont systemFontOfSize:fontSize];
    descriptionLabel.textColor = [UIColor darkGrayColor];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:descriptionLabel];
    
    // Initializing dummy text label. It is used to fix description text truncation issue
    dummyLabel = [[[JSLabel alloc] init] autorelease];
    dummyLabel.font = [UIFont systemFontOfSize:fontSize];
    dummyLabel.textColor = [UIColor darkGrayColor];
    dummyLabel.numberOfLines = 0;
    dummyLabel.backgroundColor = [UIColor clearColor];
    dummyLabel.hidden = YES;
    [self.contentView addSubview:dummyLabel];
    
    // Initializing indicator view
    _indicatorView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    [_indicatorView startAnimating];
    
    [self.contentView addSubview:_indicatorView];
    
    // Initializing image view
    _image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage"]] autorelease];
    _image.layer.cornerRadius = 5;
    _image.layer.masksToBounds = YES;
    _image.layer.borderWidth = 0;
    
    [self.contentView addSubview:_image];
    
    
    // Setting Auto Resizing mask to false for all UI elements as we are setting the constraints programmatically
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    dummyLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _image.translatesAutoresizingMaskIntoConstraints = NO;
    _indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Initializing image Constraints array
    self.imageConstraints = [NSMutableArray array];
    
    [self applyConstraints];
    
    return self;
}


#pragma mark - State

- (void)setData:(ILListItem *)item {
    
    _item = item;
    titleLabel.text = item.title;
    descriptionLabel.text = item.text;
    dummyLabel.text = @"Test Message";
    
    if (item.image) {
        _image.image  = item.image;
        _image.hidden = NO;
        
    } else {
        _image.hidden = YES;
    }
    
//    [self setNeedsUpdateConstraints];
//     [self setNeedsLayout];
}


#pragma mark - Layout

- (void)updateConstraints {
    [super updateConstraints];
    
    // add contraints if image is visible, otherwise remove them
    if (_item.image) {
        [self.contentView addConstraints:self.imageConstraints];
        
    } else {
        [_indicatorView stopAnimating];
        [self.contentView removeConstraints:self.imageConstraints];
    }
    
    [self updateGradient];
}

- (void) updateGradient {
    
    // Adding gradient
    
    CAGradientLayer *gradient = [CAGradientLayer layer];

    gradient.frame = self.bounds;
    
    // Start color is #BFBFBF and end Color is #E6E6E6
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.902 green:0.902 blue:0.902 alpha:1] CGColor], (id)[[UIColor colorWithRed:0.749 green:0.749 blue:0.749 alpha:1] CGColor], nil];
    
    [self.contentView.layer insertSublayer:gradient atIndex:0];
}

- (void)applyConstraints {
    
    // First remove all the existing constraints
    [self.contentView removeConstraints:self.contentView.constraints];
    
    
    
    // Defining Title Text Label constrains
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                                 attribute:NSLayoutAttributeLeft
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeLeft
                                                                multiplier:1
                                                                  constant:cellPadding]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1
                                                                  constant:-1*cellPadding]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeTop
                                                                multiplier:1
                                                                  constant:cellPadding]];
    
    // Defining Description Label constrains
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel
                                                                 attribute:NSLayoutAttributeLeft
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeLeft
                                                                multiplier:1
                                                                  constant:cellPadding]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1
                                                                  constant:-1 *cellPadding
                                                                  priority:UILayoutPriorityDefaultHigh]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:titleLabel
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1
                                                                  constant:cellPadding]];
    
    
    // Defining dummy text label constraints
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dummyLabel
                                                                 attribute:NSLayoutAttributeLeft
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeLeft
                                                                multiplier:1
                                                                  constant:cellPadding]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dummyLabel
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.contentView
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1
                                                                  constant:-1 *cellPadding
                                                                  priority:UILayoutPriorityDefaultHigh]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dummyLabel
                                                                 attribute:NSLayoutAttributeTop
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:descriptionLabel
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1
                                                                  constant:0]];
    
    
    // Defining content View bottom border constraint
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView
                                                                 attribute:NSLayoutAttributeBottom
                                                                 relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                    toItem:dummyLabel
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1
                                                                  constant:0]];
    
    
#pragma mark - Image related constraints
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_image
                                                                      attribute:NSLayoutAttributeCenterY
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.contentView
                                                                      attribute:NSLayoutAttributeCenterY
                                                                     multiplier:1
                                                                       constant:cellPadding]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:descriptionLabel
                                                                      attribute:NSLayoutAttributeRight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:_image
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1
                                                                       constant:-1 *cellPadding]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeBottom
                                                                      relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                         toItem:_image
                                                                      attribute:NSLayoutAttributeBottom
                                                                     multiplier:1
                                                                       constant:cellPadding
                                                                       priority:UILayoutPriorityDefaultHigh]];
    
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeRight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:_image
                                                                      attribute:NSLayoutAttributeRight
                                                                     multiplier:1
                                                                       constant:cellPadding]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_image
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:imageWidth]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_image
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:imageWidth]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_indicatorView
                                                                      attribute:NSLayoutAttributeCenterX
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:_image
                                                                      attribute:NSLayoutAttributeCenterX
                                                                     multiplier:1
                                                                       constant:0]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_indicatorView
                                                                      attribute:NSLayoutAttributeCenterY
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:_image
                                                                      attribute:NSLayoutAttributeCenterY
                                                                     multiplier:1
                                                                       constant:0]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_indicatorView
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:0]];
    
    [self.imageConstraints addObject:[NSLayoutConstraint constraintWithItem:_indicatorView
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:0]];
    
}

- (void)dealloc {
    [dummyLabel release];
    [super dealloc];
}

@end
