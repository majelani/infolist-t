//
//  ILServerDownloadManager.h
//  InfoList
//  ILServerDownloadManager provides functionality to download JSON Feed from remote server via http connection. It does not
//  process the stream, rather retrieve the data only and pass it to other objects for further processing.
//
//  Created by Aamir Jelani on 21/01/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ILServerDownloadManager : NSObject

//itemsArray and listHeading contain the data and header of downloaded information
//this information is retained here so that multiple views can access it
@property (nonatomic, strong) NSMutableArray *itemsArray;
@property (nonatomic, strong) NSString *listHeading;

+ (void) setup;
+ (void) startDownload;
+ (void) stopDownload;
+ (ILServerDownloadManager*)sharedInstance;

@end
