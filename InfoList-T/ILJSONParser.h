//
//  ILJSONParser.h
//  InfoList
//  ILJSONParser provides the functionality to process JSON feed passed to this class object in the form of NSData.
//  It processes the stream and extracts objects of type ILListItem and stores these in an array. Client can then retrieve
//  this array to further work on the objects.
//
//  Created by Aamir Jelani on 22/02/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ILJSONParser : NSOperation

// A block to call when an error is encountered during parsing.
@property (nonatomic, copy) void (^errorHandler)(NSError *error);

// NSArray containing AppRecord instances for each entry parsed
// from the input data.
// Only meaningful after the operation has completed.
@property (nonatomic, strong) NSMutableArray *itemsArray;
@property (nonatomic, strong) NSString *listHeading;

// The initializer for this NSOperation subclass.
- (instancetype)initWithData:(NSData *)data;

@end
