//
//  ILImageDownloadManager.m
//  InfoList
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import "ILImageDownloadManager.h"

// Image icon size used in List View
#define kAppIconSize 73

@interface ILImageDownloadManager ()

// activeDownload contains the data after successful download
@property (nonatomic, strong) NSMutableData *activeDownload;

// imageConnection represents the http connection used to download current image
@property (nonatomic, strong) NSURLConnection *imageConnection;

// executing indicates if the image download operation is current executing or not
@property (nonatomic, getter = isExecuting) BOOL executing;

// finished indiates if the image download operation is finished or not
@property (nonatomic, getter = isFinished)  BOOL finished;

@end
@implementation ILImageDownloadManager
@synthesize executing;
@synthesize finished;

// start is the first method called when the operation is about to execute
- (void)start
{
    if (self.isCancelled)
    {
        self.executing = NO;
        self.finished = YES;
        return;
    }
    
    self.executing = YES;
    self.finished = NO;
    
    self.activeDownload = [NSMutableData data];
    
    // Create an NSURLRequest for current image download
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0f];
    
    // Initialize NSURLConnection
    self.imageConnection = [[[NSURLConnection alloc] initWithRequest:request delegate:self  startImmediately:NO] autorelease];
    
    // Schedule the NSURLConnection to run on main loop
    [self.imageConnection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    
    // start the connection
    [self.imageConnection start];
    
}

#pragma mark NSOperation Specific Methods

// Set this to return YES if operations to be executed in parallel, else NO for a serial operation
- (BOOL)isConcurrent
{
    return YES;
}

- (void)setExecuting:(BOOL)isExecuting
{
    [self willChangeValueForKey:@"isExecuting"];
    isExecuting = isExecuting;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)setFinished:(BOOL)isFinished
{
    [self willChangeValueForKey:@"isFinished"];
    isFinished = isFinished;
    [self didChangeValueForKey:@"isFinished"];
}


#pragma mark - NSURLConnectionDelegate

// didReceiveData is called when http connection successfully returns data
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // store the data for later reference
    [self.activeDownload appendData:data];
}

// didFailWithError is called with http connection encounters any error
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.executing = NO;
    self.finished = YES;
    
    // Clear the activeDownload property to allow later attempts
    self.activeDownload = nil;
    
    // Release the connection now that it's finished
    self.imageConnection = nil;
    
    self.image = nil;
    
    // Call the delegate for failure handling
    if(self.errorHandler)
        self.errorHandler();
}

// connectionDidFinishLoading is called when http connection is completed successfully
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(self.isCancelled){
        self.errorHandler();
        return;
    }
    
    // Set image and clear temporary data/image
    UIImage *image = [[[UIImage alloc] initWithData:self.activeDownload] autorelease];
    
    if (image.size.width != kAppIconSize || image.size.height != kAppIconSize)
    {
        // If the image size if not what is desired, scale it
        CGSize itemSize = CGSizeMake(kAppIconSize, kAppIconSize);
        UIGraphicsBeginImageContextWithOptions(itemSize, NO, 0.0f);
        CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
        [image drawInRect:imageRect];
        self.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else
    {
        self.image = image;
    }
    
    self.activeDownload = nil;
    
    // call our delegate and tell it that our icon is ready for display
    if (self.completionHandler)
    {
        self.completionHandler();
    }
    
    self.imageConnection = nil;
    self.executing = NO;
    self.finished = YES;
    
}

- (void) dealloc {
    [super dealloc];
    self.imageConnection = nil;
}

@end
