//
//  ILServerDownloadManager.m
//  InfoList
//
//  Created by Aamir Jelani on 21/03/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import "ILServerDownloadManager.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "ILListItem.h"
#import "ILListViewController.h"
#import "ILJSONParser.h"
#import "ILConstants.h"
#import "AppDelegate.h"


@interface ILServerDownloadManager (){
    ILJSONParser *parser;
}

// serverConnection keeps a handle to remote server connection
@property (nonatomic, strong) NSURLConnection *serverConnection;

// listData contains all the items fetched from data stream
@property (nonatomic, strong) NSMutableData *listData;

// the queue to download data from server
@property (nonatomic, strong) NSOperationQueue *queue;

// AppDelegate pointer to access the root view controller
@property (nonatomic, strong) AppDelegate *appDelegate;


@end

@implementation ILServerDownloadManager
// web feed address
static NSString *const  serverURL =
@"https://dl.dropboxusercontent.com/u/746330/facts.json";
//@"http://infolist.netne.net/testM.php";
//@"http://192.168.0.10:8888/testM2.php";


+ (void) setup {
    [ILServerDownloadManager sharedInstance];
}

+ (void) startDownload {
    
    
    //Getting AppDelegate handle to access root view controller
    [ILServerDownloadManager sharedInstance].appDelegate = [UIApplication sharedApplication].delegate;
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:serverURL]];
    [ILServerDownloadManager sharedInstance].serverConnection = [[[NSURLConnection alloc] initWithRequest:urlRequest delegate:[ILServerDownloadManager sharedInstance]] autorelease];
    
    NSAssert([ILServerDownloadManager sharedInstance].serverConnection != nil, @"Unable to create URL connection.");
    
    
}

+ (void) stopDownload {
    [[ILServerDownloadManager sharedInstance].queue cancelAllOperations];
}

// Error handler to display a user friendly alert dialog with Error Information
- (void)handleError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Unable to connect Server.\nPlease try again."
                                                         message:errorMessage
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil] autorelease];
    [alertView show];
    
    
    
    // Send Notification for No Network connection
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNetWorkNotFound object:nil];
    
}



#pragma mark - NSURLConnectionDelegate

// This function indicates a response return from server.
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [ILServerDownloadManager sharedInstance].listData = [NSMutableData data];
}

// This function indicates a return object from server
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [[ILServerDownloadManager sharedInstance].listData appendData:data];
}

// This function gets called when the http request cannot be completed due to any error.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // handle the error
    [[ILServerDownloadManager sharedInstance] handleError:error];
    
    // release the connection
    [ILServerDownloadManager sharedInstance].serverConnection = nil;
}

// This function gets called after a successful http request
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // release the connection
    [ILServerDownloadManager sharedInstance].serverConnection = nil;
    
    // This queue will handle the data load from server operation
    [ILServerDownloadManager sharedInstance].queue = [[[NSOperationQueue alloc] init] autorelease];
    
    
    // Using
    if(parser)
        [parser autorelease];
    parser = [[ILJSONParser alloc] initWithData:[ILServerDownloadManager sharedInstance].listData];
    
    parser.errorHandler = ^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ILServerDownloadManager sharedInstance] handleError:error];
        });
    };
    
    parser.completionBlock = ^(void) {
        // Get the main thread handle and execute this completion block
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(parser.itemsArray){
                //Store title and data array in List View controller
                self.itemsArray = parser.itemsArray;
                self.listHeading  = parser.listHeading;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateInfo object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateDetailInfo object:nil];
            }
        });
        
        // we are finished with the queue and our ParseOperation
        [ILServerDownloadManager sharedInstance].queue = nil;
    };
    
    // Time to start the parsing operation.
    [[ILServerDownloadManager sharedInstance].queue addOperation:parser];
    
    [ILServerDownloadManager sharedInstance].listData = nil;
}

#pragma mark - Singleton

+ (ILServerDownloadManager*)sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static ILServerDownloadManager* _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[ILServerDownloadManager alloc] init];
    });
    return _sharedObject;
}

- (void) dealloc{
    [super dealloc];
    parser = nil;
}
@end
